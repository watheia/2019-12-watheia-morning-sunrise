module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port: env.int("PORT", 1337),
  url: "https://api.micribase.dev",
  admin: {
    auth: {
      secret: env("ADMIN_JWT_SECRET", "983d177b05326e5823311d73b7edd5d8"),
    },
  },
});
